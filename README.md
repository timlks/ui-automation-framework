# UI Automation Framework
#
# This project was created to show my knowledge on creating and using a testing framework. 
# I used the "https://angular.realworld.io/" website as a testing target, which an angular website.
# 
# Prerequisites:
# 1. Have ChromeDriver installed
# 
# Argument Options:
# You can run the tests with (optional) arguments, by adding in the command prompt or the ide running configuration one of the following arguments:
# 1. browser: It is used to choose the browser to run the tests with. It can be chrome, firefox or ie. The option is case insensitive.
# 2. profile: It is used to choose the profile to run the tests with. The option is case sensitive.
# 3. useGrid: It is used to choose whether to run the tests on remote driver or not. The option is be case insensitive
# example: -Dbrowser="chrome"
#
# Configuration:
# Config option can be configured to either use the default one, or choose a user created one. 
# To use a user created one, you will need to create an environmental variable called "testConfig" and a file called "environments.conf"
# The value of the variable will be the path to the config file. eg "../folder/environments.conf"
# 
# Note 1: Grid option is not working for now
# Note 2: Only chrome works for now