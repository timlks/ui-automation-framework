package javascript;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class AngularUtils {
	
	public static void waitForAngular(WebDriver driver) {
		driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		
		jsExecutor.executeAsyncScript(new ScriptLoader("waitForAngular").getScriptContent(), "app-root");
	}
}
