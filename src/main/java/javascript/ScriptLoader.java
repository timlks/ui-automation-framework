package javascript;

import java.io.IOException;
import java.io.InputStream;

final class ScriptLoader {
	private final String scriptResoucePath = "scripts/";
	private final transient String filename;
	
	ScriptLoader(final String file) {
		this.filename = file + ".js";
	}
	
	String getScriptContent() {
		try {
			final InputStream stream = ScriptLoader.class.getClassLoader()
					.getResourceAsStream(scriptResoucePath + filename);
			final byte[] bytes = new byte[stream.available()];
			stream.read(bytes);
			return new String(bytes, "UTF-8");
		} catch (IOException e) {
			return null;
		}
	}
}
