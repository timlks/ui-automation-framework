package datagenerators;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * 
 * @author TimothLks
 *
 *         Contains methods that help generating random values
 */
public class RandomValuesGenerator {
	
	/**
	 * Creates a random generated String
	 * 
	 * @return a random String
	 */
	public static String randomString() {
		return RandomStringUtils.randomAlphanumeric(randomInt(10, 15)).toLowerCase();
	}
	
	public static int randomInt(int min, int max) {
		Random random = new Random();
		return random.nextInt(max + 1 - min) + min;
	}
	
	/**
	 * Creates a random generated email
	 * 
	 * @param minLength
	 *            The minimum length of the
	 * @param maxLength
	 *            The maximum length of the
	 * 
	 * @return a random email
	 */
	public static String randomEmail(int minLength, int maxLength) {
		return randomString() + "@test.com";
	}
}
