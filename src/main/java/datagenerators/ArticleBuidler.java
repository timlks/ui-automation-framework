package datagenerators;

import dataobjetcs.Article;

public class ArticleBuidler {
	
	private String title;
	private String description;
	private String body;
	private String[] tags;
	
	public static ArticleBuidler getArticle() {
		return new ArticleBuidler();
	}
	
	public ArticleBuidler withTitle(String title) {
		this.title = title;
		return this;
	}
	
	public ArticleBuidler withDescription(String description) {
		this.description = description;
		return this;
	}
	
	public ArticleBuidler withBody(String body) {
		this.body = body;
		return this;
	}
	
	public ArticleBuidler withTags(String[] tags) {
		this.tags = tags;
		return this;
	}
	
	public Article build() {
		return new Article(title, description, body, tags);
	}
}
