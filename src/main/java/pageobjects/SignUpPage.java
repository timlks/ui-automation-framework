package pageobjects;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import dataobjetcs.UserProfile;

/**
 * 
 * @author U.7831212
 *
 *         Page Object for Sign Up Page
 */
public class SignUpPage {
	
	private WebDriver driver;
	
	@FindBy(xpath = "//input[@formcontrolname='username']")
	WebElement username;
	
	@FindBy(xpath = "//input[@formcontrolname='email']")
	WebElement email;
	
	@FindBy(xpath = "//input[@formcontrolname='password']")
	WebElement password;
	
	@FindBy(xpath = "//form/fieldset/button")
	WebElement signUpButton;
	
	public SignUpPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	/**
	 * @param usernameToEnter
	 *            the username to set
	 */
	public void setUsername(String usernameToEnter) {
		try {
			username.clear();
			username.sendKeys(usernameToEnter);
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Username field not located");
		}
	}
	
	/**
	 * @param emailToEnter
	 *            the email to set
	 */
	public void setEmail(String emailToEnter) {
		try {
			email.clear();
			email.sendKeys(emailToEnter);
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Email field not located");
		}
	}
	
	/**
	 * @param passwordToEnter
	 *            the password to set
	 */
	public void setPassword(String passwordToEnter) {
		try {
			password.clear();
			password.sendKeys(passwordToEnter);
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Password field not located");
		}
	}
	
	/**
	 * Clicks the sign up button
	 */
	public void clickSignUp() {
		try {
			signUpButton.click();
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Sign Up field not located");
		}
	}
	
	/**
	 * Creates a user
	 * 
	 * @param userProfile
	 *            The profile of the user that is being created
	 */
	public void createUser(UserProfile userProfile) {
		setUsername(userProfile.getUsername());
		setPassword(userProfile.getPassword());
		setEmail(userProfile.getEmail());
		clickSignUp();
	}
}
