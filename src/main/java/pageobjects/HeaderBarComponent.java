package pageobjects;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * 
 * @author U.7831212
 *
 *         Page Object for Header Bar
 */
public class HeaderBarComponent {
	
	private WebDriver driver;
	
	@FindBy(xpath = "//nav/div/ul/li[4]/a")
	WebElement profileName;
	
	@FindBy(xpath = "//nav/div/ul/li[3]/a")
	WebElement settings;
	
	@FindBy(xpath = "//nav/div/ul/li[2]/a")
	WebElement newArticle;
	
	@FindBy(xpath = "//nav/div/ul/li[2]/a")
	WebElement signIn;
	
	@FindBy(xpath = "//nav/div/ul/li[3]/a")
	WebElement signUp;
	
	@FindBy(xpath = "//nav/div/ul/li[1]/a")
	WebElement home;
	
	public HeaderBarComponent(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	/**
	 * @return the home button
	 */
	public WebElement getHome() {
		return home;
	}
	
	/**
	 * Checks if the settings button is enabled
	 * 
	 * @return true is settings button is active, and false if it isn't
	 */
	public boolean isSettingsButtonActive() {
		return settings.isEnabled();
	}
	
	/**
	 * Clicks the settings button
	 */
	public void clickSettings() {
		try {
			settings.click();
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Settings field not located");
		}
	}
	
	/**
	 * Clicks the new article button
	 */
	public void clickNewArticle() {
		try {
			newArticle.click();
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Settings field not located");
		}
	}
	
	/**
	 * Clicks the sign in button
	 */
	public void clickSignIn() {
		try {
			signIn.click();
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Settings field not located");
		}
	}
	
	/**
	 * Clicks the sign up button
	 */
	public void clickSignUp() {
		try {
			signUp.click();
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Settings field not located");
		}
	}
	
	/**
	 * @return true if profile name is displayed, and false if it is not displayed
	 */
	public boolean isPProfileNameDisplayed() {
		return profileName.isDisplayed();
	}
	
	/**
	 * @return the profile name
	 */
	public String getProfileName() {
		return profileName.getText();
	}
}
