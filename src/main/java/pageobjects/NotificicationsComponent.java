package pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * 
 * @author U.7831212
 *
 *         Page Object for Notifications
 */
public class NotificicationsComponent {
	
	private WebDriver driver;
	
	@FindBy(className = "error-messages")
	WebElement notifications;
	
	public NotificicationsComponent(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	/**
	 * Checks if any notifications are displayed
	 * 
	 * @return true if any notification is displayed, and false if it is not
	 *         displayed
	 */
	public Boolean areNotificationsDisplayed() {
		if (getNotifications().size() > 0) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Checks if a given specification is displayed
	 * 
	 * @param notification
	 *            The notification that is being checked if it is displayed
	 * 
	 * @return true if notification is displayed, and false if it is not displayed
	 */
	public Boolean isNotificationDisplayed(String notification) {
		for (WebElement notificationDisplayed : getNotifications()) {
			if (notificationDisplayed.getText().equals(notification)) {
				return true;
			}
		}
		
		return false;
	}
	
	public List<WebElement> getNotifications() {
		return notifications.findElements(By.tagName("li"));
	}
}
