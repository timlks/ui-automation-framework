package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * 
 * @author U.7831212
 *
 *         Page Object for New Article Page
 */
public class NewArticlePage {
	
	private WebDriver driver;
	
	@FindBy(xpath = "//input[@formcontrolname='title']")
	WebElement title;
	
	@FindBy(xpath = "//input[@formcontrolname='description']")
	WebElement description;
	
	@FindBy(xpath = "//textarea[@formcontrolname='body']")
	WebElement body;
	
	@FindBy(xpath = "//fieldset[4]/input")
	WebElement tags;
	
	@FindBy(xpath = "//form/fieldset/button")
	WebElement publishButton;
	
	public NewArticlePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	/**
	 * Clicks the publish button
	 */
	public void clickPublishButton() {
		try {
			publishButton.click();
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Publish field not located");
		}
	}
	
	/**
	 * @param titleToEnter
	 *            the title to set
	 */
	public void setTitle(String titleToEnter) {
		try {
			title.clear();
			title.sendKeys(titleToEnter);
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Title field not located");
		}
	}
	
	/**
	 * @param descriptionToEnter
	 *            the description to set
	 */
	public void setDescription(String descriptionToEnter) {
		try {
			description.clear();
			description.sendKeys(descriptionToEnter);
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Description field not located");
		}
	}
	
	/**
	 * @param bodyToEnter
	 *            the body to set
	 */
	public void setBody(String bodyToEnter) {
		try {
			body.clear();
			body.sendKeys(bodyToEnter);
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Body field not located");
		}
	}
	
	/**
	 * @param tagsToEnter
	 *            the tags to set
	 */
	public void setTags(String[] tagsToEnter) {
		try {
			tags.clear();
			
			for (String tag : tagsToEnter) {
				tags.sendKeys(tag);
				tags.sendKeys(Keys.ENTER);
			}
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Tags field not located");
		}
	}
	
}
