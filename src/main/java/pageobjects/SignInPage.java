package pageobjects;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import dataobjetcs.UserProfile;

/**
 * 
 * @author U.7831212
 *
 *         Page Object for Sign In Page
 */
public class SignInPage {
	
	private WebDriver driver;
	
	@FindBy(xpath = "//input[@formcontrolname='email']")
	WebElement email;
	
	@FindBy(xpath = "//input[@formcontrolname='password']")
	WebElement password;
	
	@FindBy(xpath = "//form/fieldset/button")
	WebElement signInButton;
	
	public SignInPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	/**
	 * @return the signInButton
	 */
	public WebElement getSignInButton() {
		return signInButton;
	}
	
	/**
	 * @param emailToEnter
	 *            the email to set
	 */
	public void setEmail(String emailToEnter) {
		try {
			email.clear();
			email.sendKeys(emailToEnter);
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Email field not located");
		}
	}
	
	/**
	 * @param passwordToEnter
	 *            the password to set
	 */
	public void setPassword(String passwordToEnter) {
		try {
			password.clear();
			password.sendKeys(passwordToEnter);
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Password field not located");
		}
	}
	
	/**
	 * Clicks the sign in button
	 */
	public void clickSignIn() {
		try {
			signInButton.click();
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Sign In field not located");
		}
	}
	
	/**
	 * Adds email and password
	 * 
	 * @param userProfile
	 *            The profile of the user that is trying to sign in
	 */
	public void addCredentials(UserProfile userProfile) {
		setEmail(userProfile.getEmail());
		setPassword(userProfile.getPassword());
	}
}
