package pageobjects;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SettingsPage {
	
	private WebDriver driver;
	
	@FindBy(className = "text-xs-center")
	WebElement signUpLabel;
	
	@FindBy(xpath = "//div/div/div/div/button")
	WebElement logoutButton;
	
	public SettingsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	/**
	 * Clicks the logout button
	 */
	public void clickLogout() {
		try {
			logoutButton.click();
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Logout field not located");
		}
	}
	
}
