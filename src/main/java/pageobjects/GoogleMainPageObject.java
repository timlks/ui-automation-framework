package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleMainPageObject
{
	private WebDriver driver;
	
	@FindBy(xpath = "//*[@id=\"tsf\"]/div[2]/div/div[3]/center/input[1]")
	WebElement searchButton;
	
	public GoogleMainPageObject(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public Boolean isGoogleSearchButtonDisplayed()
	{
		return searchButton.isDisplayed();
	}
}
