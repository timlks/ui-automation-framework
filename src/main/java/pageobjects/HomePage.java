package pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	private WebDriver driver;
	
	@FindBy(xpath = "//div[1]/div/ul/li[1]/a")
	WebElement yourFeed;
	
	@FindBy(xpath = "//div[1]/div/ul/li[2]/a")
	WebElement globalFeed;
	
	@FindBy(xpath = "//app-article-list")
	WebElement articleList;
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	/**
	 * Gets the list of articles
	 * 
	 * @return a list of articles
	 */
	public List<WebElement> getArticleList() {
		List<WebElement> list = new ArrayList<WebElement>();
		
		list.add(articleList.findElement(By.xpath(".//app-article-preview")));
		
		return list;
	}
	
	/**
	 * Checks the article list for an article with a specific title
	 * 
	 * @param title
	 *            The title of the article we are looking for
	 * 
	 * @return true if the article is found, and false if it isn't
	 */
	public boolean containsArticle(String title) {
		for (WebElement article : getArticleList()) {
			if (article.findElement(By.xpath(".//h1")).getText().equals(title)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Selects a feed
	 * 
	 * @param feed
	 *            The feed to be selected
	 */
	public void selectFeed(String feed) {
		try {
			if (feed.equals("your")) {
				yourFeed.click();
			} else {
				globalFeed.click();
			}
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Field not located");
		}
	}
	
}
