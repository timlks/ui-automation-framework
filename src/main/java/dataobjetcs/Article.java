package dataobjetcs;

public class Article {
	
	private String title;
	private String description;
	private String body;
	private String[] tags;
	
	public Article(String title, String description, String body, String[] tags) {
		super();
		this.title = title;
		this.description = description;
		this.body = body;
		this.tags = tags;
	}
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * @param title
	 *            the title to set
	 */
	public void withTitle(String title) {
		this.title = title;
	}
	
	/**
	 * @return the about
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param about
	 *            the about to set
	 */
	public void withDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}
	
	/**
	 * @param body
	 *            the body to set
	 */
	public void withBody(String body) {
		this.body = body;
	}
	
	/**
	 * @return the tags
	 */
	public String[] getTags() {
		return tags;
	}
	
	/**
	 * @param tags
	 *            the tags to set
	 */
	public void withTags(String[] tags) {
		this.tags = tags;
	}
	
}
