package dataobjetcs;

import java.util.Map;

public class Pages {
	
	private Map<String, Object> pages;
	
	public Pages() {
		super();
	}
	
	public Pages(Map<String, Object> pages) {
		super();
		this.pages = pages;
	}
	
	/**
	 * @return the pages
	 */
	public Map<String, Object> getPages() {
		return pages;
	}
	
	/**
	 * @param pages
	 *            the pages to set
	 */
	public void setPages(Map<String, Object> pages) {
		this.pages = pages;
	}
	
}
