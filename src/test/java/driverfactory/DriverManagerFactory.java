package driverfactory;

import org.openqa.selenium.remote.DesiredCapabilities;

import driverfactory.drivermanagers.ChromeDriverManager;
import driverfactory.drivermanagers.FirefoxDriverManager;
import driverfactory.drivermanagers.RemoteDriverManager;

public class DriverManagerFactory {
	
	public static DriverManager getLocalManager(DriverConfiguration configuration) {
		
		DriverManager driverManager = null;
		
		switch (configuration.getDriverType()) {
		case CHROME:
			driverManager = new ChromeDriverManager(configuration);
			break;
		case FIREFOX:
			driverManager = new FirefoxDriverManager(configuration);
			break;
		default:
			driverManager = new ChromeDriverManager(configuration);
		}
		
		return driverManager;
	}
	
	public static DriverManager getRemoteManager(DriverConfiguration configuration) {
		
		DriverManager driverManager = null;
		DesiredCapabilities capabilities;
		
		switch (configuration.getDriverType()) {
		case CHROME:
			capabilities = DesiredCapabilities.chrome();
			driverManager = new RemoteDriverManager(configuration, capabilities);
			break;
		case FIREFOX:
			capabilities = DesiredCapabilities.firefox();
			driverManager = new RemoteDriverManager(configuration, capabilities);
			break;
		default:
			capabilities = DesiredCapabilities.chrome();
			driverManager = new RemoteDriverManager(configuration, capabilities);
		}
		
		return driverManager; 
	}
}
