package driverfactory;

import org.openqa.selenium.Platform;

public class DriverConfiguration {
	
	private DriverType driverType;
	private Boolean isHeadless;
	private String browserVersion;
	private Platform platform;
	private String hubURL;
	
	public DriverConfiguration(DriverType driverType, Boolean isHeadless) {
		super();
		this.driverType = driverType;
		this.isHeadless = isHeadless;
	}
	
	public DriverConfiguration(DriverType driverType, Boolean isHeadless, String browserVersion, Platform platform,
			String hubURL) {
		super();
		this.driverType = driverType;
		this.isHeadless = isHeadless;
		this.browserVersion = browserVersion;
		this.platform = platform;
		this.hubURL = hubURL;
	}
	
	/**
	 * @return the driverType
	 */
	public DriverType getDriverType() {
		return driverType;
	}
	
	/**
	 * @param driverType
	 *            the driverType to set
	 */
	public void setDriverType(DriverType driverType) {
		this.driverType = driverType;
	}
	
	/**
	 * @return the isHeadless
	 */
	public Boolean getIsHeadless() {
		return isHeadless;
	}
	
	/**
	 * @param isHeadless
	 *            the isHeadless to set
	 */
	public void setIsHeadless(Boolean isHeadless) {
		this.isHeadless = isHeadless;
	}
	
	/**
	 * @return the browserVersion
	 */
	public String getBrowserVersion() {
		return browserVersion;
	}
	
	/**
	 * @param browserVersion
	 *            the browserVersion to set
	 */
	public void setBrowserVersion(String browserVersion) {
		this.browserVersion = browserVersion;
	}
	
	/**
	 * @return the platform
	 */
	public Platform getPlatform() {
		return platform;
	}
	
	/**
	 * @param platform
	 *            the platform to set
	 */
	public void setPlatform(Platform platform) {
		this.platform = platform;
	}
	
	/**
	 * @return the hubURL
	 */
	public String getHubURL() {
		return hubURL;
	}
	
	/**
	 * @param hubURL
	 *            the hubURL to set
	 */
	public void setHubURL(String hubURL) {
		this.hubURL = hubURL;
	}
	
}
