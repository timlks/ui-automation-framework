package driverfactory.drivermanagers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import driverfactory.DriverConfiguration;
import driverfactory.DriverManager;

public class FirefoxDriverManager extends DriverManager {
	
	private DriverConfiguration configuration;
	
	public FirefoxDriverManager(DriverConfiguration configuration) {
		this.configuration = configuration;
	}
	
	@Override
	public void createDriver() {
		if (configuration.getIsHeadless()==false) {
			final FirefoxOptions options = new FirefoxOptions();
			options.addArguments("disable-infobars");
			driver = new ThreadLocal<WebDriver>() {
				@Override
				protected WebDriver initialValue() {
					return new FirefoxDriver(options);
				}
			};
		} else {
			final FirefoxOptions options = new FirefoxOptions();
			options.addArguments("--headless");
			driver = new ThreadLocal<WebDriver>() {
				@Override
				protected WebDriver initialValue() {
					return new FirefoxDriver(options);
				}
			};
		}
	}
}
