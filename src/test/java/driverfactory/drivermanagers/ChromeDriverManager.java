package driverfactory.drivermanagers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import driverfactory.DriverConfiguration;
import driverfactory.DriverManager;

public class ChromeDriverManager extends DriverManager {
	
	private DriverConfiguration configuration;
	
	public ChromeDriverManager(DriverConfiguration configuration) {
		this.configuration = configuration;
	}
	
	@Override
	public void createDriver() {
		if (configuration.getIsHeadless()==false) {
			final ChromeOptions options = new ChromeOptions();
			options.addArguments("disable-infobars");
			driver = new ThreadLocal<WebDriver>() {
				@Override
				protected WebDriver initialValue() {
					return new ChromeDriver(options);
				}
			};
		} else {
			final ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ThreadLocal<WebDriver>() {
				@Override
				protected WebDriver initialValue() {
					return new ChromeDriver(options);
				}
			};
		}
	}
}
