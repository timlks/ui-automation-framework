package driverfactory.drivermanagers;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import driverfactory.DriverConfiguration;
import driverfactory.DriverManager;

public class RemoteDriverManager extends DriverManager {
	
	private DriverConfiguration configuration;
	private DesiredCapabilities capabilities;
	
	public RemoteDriverManager(DriverConfiguration configuration, DesiredCapabilities capabilities) {
		this.configuration = configuration;
		this.capabilities = capabilities;
	}
	
	@Override
	public void createDriver() {
		
		capabilities.setPlatform(configuration.getPlatform());
		capabilities.setVersion(configuration.getBrowserVersion());
		
		driver = new ThreadLocal<WebDriver>() {
			@Override
			protected WebDriver initialValue() {
				try {
					return new RemoteWebDriver(new URL(configuration.getHubURL()), capabilities);
				} catch (MalformedURLException e) {
					return null;
				}
			}
		};
	}
}
