package navigation;

import javascript.AngularUtils;
import pageobjects.HeaderBarComponent;
import session.Session;

public class PageSelection {
	
	public static void goToPage(Session session, String page) {
		
		HeaderBarComponent headerBarComponent = new HeaderBarComponent(session.getDriver());
		session.getDriver().navigate().to(session.getBaseUrl());
		
		AngularUtils.waitForAngular(session.getDriver());
		
		if (page.equals("home")) {
			session.getDriver().navigate().to(session.getBaseUrl());
		} else if (page.equals("signIn")) {
			headerBarComponent.clickSignIn();
		} else if (page.equals("signUp")) {
			headerBarComponent.clickSignUp();
		} else if (page.equals("newArticle")) {
			headerBarComponent.clickNewArticle();
		}
	}
}
