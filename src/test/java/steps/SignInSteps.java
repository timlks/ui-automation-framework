package steps;

import static org.assertj.core.api.Assertions.assertThat;

import java.net.MalformedURLException;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import datagenerators.RandomValuesGenerator;
import javascript.AngularUtils;
import navigation.PageSelection;
import pageobjects.NotificicationsComponent;
import pageobjects.SignInPage;
import session.SharedDataKeys;

public class SignInSteps extends StepBase {
	
	private SignInPage signInPage;
	private NotificicationsComponent notificicationsComponent;
	
	@Before
	public void setUp() throws MalformedURLException {
		signInPage = new SignInPage(session.getDriver());
		notificicationsComponent = new NotificicationsComponent(session.getDriver());
	}
	
	@When("^the user signs in with valid information$")
	public void the_user_signs_in_with_valid_information() throws Throwable {
		
		if (!session.getDriver().getCurrentUrl().contains("login")) {
			PageSelection.goToPage(session, "signIn");
		}
		
		signInPage.setEmail(session.getData(SharedDataKeys.EMAIL));
		signInPage.setPassword(session.getData(SharedDataKeys.PASSWORD));
		
		AngularUtils.waitForAngular(session.getDriver());
		
		signInPage.clickSignIn();
		
		AngularUtils.waitForAngular(session.getDriver());
	}
	
	@When("^the user enters wrong credentials$")
	public void the_user_enters_wrong_credentials() throws Throwable {
		
		if (!session.getDriver().getCurrentUrl().contains("login")) {
			PageSelection.goToPage(session, "signIn");
		}
		
		signInPage.setEmail(RandomValuesGenerator.randomEmail(5, 10));
		signInPage.setPassword(RandomValuesGenerator.randomString());
		
		AngularUtils.waitForAngular(session.getDriver());
		
		signInPage.clickSignIn();
		
		AngularUtils.waitForAngular(session.getDriver());
	}
	
	@Then("^a notification saying \"([^\"]*)\" is displayed$")
	public void a_notification_saying_is_displayed(String notification) throws Throwable {
		assertThat(notificicationsComponent.areNotificationsDisplayed()).isTrue();
		assertThat(notificicationsComponent.isNotificationDisplayed(notification)).isTrue();
	}
	
}
