package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import javascript.AngularUtils;
import pageobjects.HeaderBarComponent;
import pageobjects.HomePage;
import session.SharedDataKeys;

public class HomePageSteps extends StepBase {
	
	private HomePage homePage;
	private HeaderBarComponent headerBarComponent;
	
	@Before
	public void setUp() throws MalformedURLException {
		headerBarComponent = new HeaderBarComponent(session.getDriver());
		homePage = new HomePage(session.getDriver());
	}
	
	@After
	public void tearDown() throws MalformedURLException {
		session.stopDriver();
	}
	
	@When("^selects \"([^\"]*)\" feed$")
	public void selects_feed(String feed) throws Throwable {
		
		AngularUtils.waitForAngular(session.getDriver());
		
		homePage.selectFeed(feed);
	}
	
	@Then("^the article is displayed$")
	public void the_article_is_displayed() throws Throwable {
		
		AngularUtils.waitForAngular(session.getDriver());
		
		assertTrue(homePage.containsArticle(session.getData(SharedDataKeys.TITLE)));
	}
	
	@Then("^the user is signed in$")
	public void the_user_is_logged_in() throws Throwable {
		
		AngularUtils.waitForAngular(session.getDriver());
				
		assertTrue(headerBarComponent.isPProfileNameDisplayed());
		assertEquals(session.getData(SharedDataKeys.USERNAME), headerBarComponent.getProfileName());
	}
}
