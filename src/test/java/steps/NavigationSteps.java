package steps;

import cucumber.api.java.en.Given;
import navigation.PageSelection;

public class NavigationSteps extends StepBase {
	
	@Given("^the user navigates to the \"([^\"]*)\" page$")
	public void the_user_navigates_to_the_page(String page) throws Throwable {
		PageSelection.goToPage(session, page);
	}
}
