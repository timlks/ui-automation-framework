package steps;

import static org.junit.Assert.fail;

import java.net.MalformedURLException;

import session.Session;
import session.SessionSingleton;

/**
 * 
 * @author Timothy Lks
 *
 *         Base class for classes that implement steps
 */
public class StepBase {
	
	protected Session session;
	
	public StepBase()
	{
		try {
			session = SessionSingleton.getSession();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			fail("Unable to initialise session: " + e.getMessage());
		}
	}
	
}
