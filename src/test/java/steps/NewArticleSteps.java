package steps;

import java.net.MalformedURLException;
import java.util.Map;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.When;
import datagenerators.ArticleBuidler;
import dataobjetcs.Article;
import javascript.AngularUtils;
import pageobjects.NewArticlePage;
import session.SharedDataKeys;

public class NewArticleSteps extends StepBase {
	
	private NewArticlePage newArticlePage;
	
	@Before
	public void setUp() throws MalformedURLException {
		newArticlePage = new NewArticlePage(session.getDriver());
	}
	
	@After
	public void tearDown() throws MalformedURLException {
		session.stopDriver();
	}
	
	@When("^the user creates a new article with the following information$")
	public void the_user_creates_a_new_article_with_the_following_information(Map<String, String> data)
			throws Throwable {
		
		AngularUtils.waitForAngular(session.getDriver()); 
		
		String[] tags = data.get("tags").split(",");	
		Article article = ArticleBuidler.getArticle().withTitle(data.get("title")).withDescription(data.get("description"))
				.withBody(data.get("body")).withTags(tags).build();
		
		newArticlePage.setTitle(article.getTitle());
		newArticlePage.setDescription(article.getDescription());
		newArticlePage.setBody(article.getBody());
		newArticlePage.setTags(article.getTags());
		
		session.addData(SharedDataKeys.TITLE, article.getTitle());
		
		AngularUtils.waitForAngular(session.getDriver()); 
		
		newArticlePage.clickPublishButton();
	}
}
