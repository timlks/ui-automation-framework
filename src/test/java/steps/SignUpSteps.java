package steps;

import java.net.MalformedURLException;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import datagenerators.RandomValuesGenerator;
import dataobjetcs.UserProfile;
import javascript.AngularUtils;
import navigation.PageSelection;
import pageobjects.HeaderBarComponent;
import pageobjects.SettingsPage;
import pageobjects.SignUpPage;
import session.SharedDataKeys;

public class SignUpSteps extends StepBase {
	
	private SignUpPage signUpPage;
	private HeaderBarComponent headerBarComponent;
	private SettingsPage settingsPage;
	
	@Before
	public void setUp() throws MalformedURLException {
		signUpPage = new SignUpPage(session.getDriver());
		headerBarComponent = new HeaderBarComponent(session.getDriver());
		settingsPage = new SettingsPage(session.getDriver());
	}
	
	@After
	public void tearDown() throws MalformedURLException {
		session.stopDriver();
	}
	
	@Given("^a user was successfully created$")
	public void a_user_was_successfully_created() {
		
		PageSelection.goToPage(session, "signUp");
		
		UserProfile userProfile = new UserProfile(RandomValuesGenerator.randomString(),
				RandomValuesGenerator.randomEmail(5, 5), RandomValuesGenerator.randomString());
		
		session.addData(SharedDataKeys.USERNAME, userProfile.getUsername());
		session.addData(SharedDataKeys.PASSWORD, userProfile.getPassword());
		session.addData(SharedDataKeys.EMAIL, userProfile.getEmail());
		
		signUpPage.createUser(userProfile);
		
		AngularUtils.waitForAngular(session.getDriver());
		
		headerBarComponent.clickSettings();
		settingsPage.clickLogout();
	}
	
	@When("^the user adds valid information$")
	public void the_user_adds_valid_information() throws Throwable {
		
		UserProfile userProfile = new UserProfile(RandomValuesGenerator.randomString(),
				RandomValuesGenerator.randomEmail(5, 5), RandomValuesGenerator.randomString());
		
		session.addData(SharedDataKeys.USERNAME, userProfile.getUsername());
		session.addData(SharedDataKeys.PASSWORD, userProfile.getPassword());
		session.addData(SharedDataKeys.EMAIL, userProfile.getEmail());
		
		signUpPage.createUser(userProfile);
	}
	
}
