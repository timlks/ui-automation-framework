package session;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;

import configuration.BrowserProfileConfig;
import configuration.EnvironmentConfig;
import configuration.GridConfig;
import driverfactory.DriverConfiguration;
import driverfactory.DriverManager;
import driverfactory.DriverManagerFactory;
import driverfactory.DriverType;

/**
 * 
 * @author Timothy Lks
 *
 */
@Component
public class Session {
	private DriverManager driverManager;
	private Boolean useGrid = false;
	private DriverConfiguration driverConfiguration;
	private EnvironmentConfig configuration;
	private BrowserProfileConfig browserProfileConfig;
	private Map<String, Object> sharedData;
	private DriverType driverType;
	
	public Session() {
		configuration = new EnvironmentConfig();
		browserProfileConfig = new BrowserProfileConfig(System.getProperty("profile", "NonHeadless"));
		useGrid = Boolean.parseBoolean(System.getProperty("useGrid", "false"));
		driverType = configureDriverType();
		driverManager = createManager();
		sharedData = new HashMap<>();
	}
	
	/**
	 * Creates a DriverManager object, which can be local or remote
	 * 
	 * @return a DriverManager object
	 */
	public DriverManager createManager() {
		if (useGrid == false) {
			driverConfiguration = new DriverConfiguration(driverType, browserProfileConfig.getIsHeadless());
			return driverManager = DriverManagerFactory.getLocalManager(driverConfiguration);
		} else {
			GridConfig gridConfig = new GridConfig();
			driverConfiguration = new DriverConfiguration(driverType, browserProfileConfig.getIsHeadless(),
					browserProfileConfig.getBrowserVersion(), gridConfig.getPlatform(), gridConfig.getHubUrl());
			return driverManager = DriverManagerFactory.getRemoteManager(driverConfiguration);
		}
	}
	
	/**
	 * Getter for the WebDriver instance associated with this session
	 * 
	 * @return the current web driver instance
	 */
	public WebDriver getDriver() {
		return driverManager.getDriver();
	}
	
	/**
	 * Quit the WebDriver instance associated with this session
	 * 
	 * @return the current web driver instance
	 */
	public void stopDriver() {
		driverManager.stopDriver();
		sharedData = new HashMap<>();
	}
	
	/**
	 * @return the configuration
	 */
	public EnvironmentConfig getConfiguration() {
		return configuration;
	}
	
	/**
	 * @return the BASE URL
	 */
	public String getBaseUrl() {
		return configuration.getBaseUrl();
	}
	
	public void addData(String key, String value) {
		sharedData.put(key, value);
	}
	
	public String getData(String key) {
		return sharedData.get(key).toString();
	}
	
	public DriverType configureDriverType() {
		
		String type = System.getProperty("browser");
		
		if (type == null) {
			return DriverType.CHROME;
		}
		
		switch (System.getProperty("browser").toLowerCase()) {
		case "chrome":
			return DriverType.CHROME;
		case "firefox":
			return DriverType.FIREFOX;
		case "ie":
			return DriverType.IE;
		default:
			return DriverType.CHROME;
		}
	}
	
}
