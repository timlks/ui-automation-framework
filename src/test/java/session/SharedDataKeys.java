package session;

public class SharedDataKeys {
	
	//User related
	public static final String USERNAME = "username";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	
	//Article related
	public static final String TITLE = "title";
}
