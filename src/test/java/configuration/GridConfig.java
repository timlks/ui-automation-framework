package configuration;

import java.io.File;

import org.openqa.selenium.Platform;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class GridConfig extends ConfigurationManager {
	
	private Config config;
	private Platform platform;
	private String hubDomain;
	private int hubPort;
	private File configFile;
	
	public GridConfig() {
		this.configFile = getConifgFile();
		loadConfigFile();
		loadConfiguration();
	}
	
	public void loadConfigFile() {
		config = ConfigFactory.parseFile(configFile).getConfig("gridConfig").resolve();
	}
	
	@Override
	public void loadConfiguration() {
		platform = configurePlatform();
		hubDomain = config.getString("hub-domain");
		hubPort = config.getInt("hub-port");
	}
	
	public Platform configurePlatform() {
		Platform result = null;
		
		String configuredPlatform = config.getString("platform");
		
		switch (configuredPlatform) {
		case ("windows"):
			result = Platform.WINDOWS;
		default:
			result = Platform.WINDOWS;
		}
		
		return result;
	}
	
	/**
	 * Returns the url to the remote driver hub
	 * 
	 * @return String containing the URL to the remote driver Hub
	 */
	
	public String getHubUrl() {
		return String.format("%s:%d/wd/hub", hubDomain, hubPort);
	}
	
	/**
	 * @return the platform
	 */
	public Platform getPlatform() {
		return platform;
	}
}
