package configuration;

import java.io.File;

import org.openqa.selenium.Platform;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class BrowserProfileConfig extends ConfigurationManager {
	
	private Config config;
	private Boolean isHeadless;
	private String browserVersion;
	private File configFile;
	
	public BrowserProfileConfig(String profile) {
		this.configFile = getConifgFile();
		loadConfigFile(profile);
		loadConfiguration();
	}
	
	public void loadConfigFile(String profile) {
		config = ConfigFactory.parseFile(configFile).getConfig(profile).resolve();
	}
	
	@Override
	public void loadConfiguration() {
		isHeadless = config.getBoolean("run-headless");
		browserVersion = config.getString("browser-version");
	}
	
	public Platform configurePlatform() {
		Platform result = null;
		
		String configuredPlatform = config.getString("platform");
		
		switch (configuredPlatform) {
		case ("windows"):
			result = Platform.WINDOWS;
		default:
			result = Platform.WINDOWS;
		}
		
		return result;
	}
	
	/**
	 * @return the isHeadless
	 */
	public Boolean getIsHeadless() {
		return isHeadless;
	}
	
	/**
	 * @return the browserVersion
	 */
	public String getBrowserVersion() {
		return browserVersion;
	}
	
}
