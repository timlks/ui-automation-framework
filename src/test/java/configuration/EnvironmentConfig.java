package configuration;

import java.io.File;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class EnvironmentConfig extends ConfigurationManager {
	
	private Config config;
	private String baseUrl;
	private File configFile;
	
	public EnvironmentConfig() {
		this.configFile = getConifgFile();
		loadConfigFile();
		loadConfiguration();
	}
	
	public void loadConfigFile() {
		config = ConfigFactory.parseFile(configFile).getConfig("vdiLocal").resolve();
	}
	
	@Override
	public void loadConfiguration() {
		baseUrl = config.getString("base-url");
	}
	
	/**
	 * @return the envUrl
	 */
	public String getBaseUrl() {
		return baseUrl;
	}
	
}
