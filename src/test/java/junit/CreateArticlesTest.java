package junit;

import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import datagenerators.RandomValuesGenerator;
import dataobjetcs.UserProfile;
import javascript.AngularUtils;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import navigation.PageSelection;
import pageobjects.HeaderBarComponent;
import pageobjects.HomePage;
import pageobjects.NewArticlePage;
import pageobjects.SignUpPage;
import session.Session;
import session.SessionSingleton;

/**
 * 
 * @author Timothy Lks
 *
 *         Contains test to check the creation of multiple articles
 */
@RunWith(JUnitParamsRunner.class)
public class CreateArticlesTest {
	
	private static Session session;
	private static SignUpPage signUpPage;
	private static HeaderBarComponent headerBarComponent;
	private static NewArticlePage newArticlePage;
	private static HomePage homePage;
	
	@BeforeClass
	public static void setUp() throws MalformedURLException {
		session = SessionSingleton.getSession();
		headerBarComponent = new HeaderBarComponent(session.getDriver());
		signUpPage = new SignUpPage(session.getDriver());
		newArticlePage = new NewArticlePage(session.getDriver());
		homePage = new HomePage(session.getDriver());
		
		PageSelection.goToPage(session, "signUp");
		
		UserProfile userProfile = new UserProfile(RandomValuesGenerator.randomString(),
				RandomValuesGenerator.randomEmail(5, 5), RandomValuesGenerator.randomString());
		
		signUpPage.createUser(userProfile);
		
		AngularUtils.waitForAngular(session.getDriver());
		
		headerBarComponent.clickNewArticle();
	}
	
	@AfterClass
	public static void tearDown() {
		session.stopDriver();
	}
	
	/**
	 * Creates multiple users based on a given .csv file
	 * 
	 * @param title
	 *            the title of the article to be created
	 * @param description
	 *            the description of the article to be created
	 * @param body
	 *            the body of the article to be created
	 */
	@Test
	@Parameters({ "title 1 | description 1 | body 1", "title 2 | description 2 | body 2",
			"title 3 | description 3 | body 3", "title 4 | description 4 | body 4",
			"title 5 | description 5 | body 5" })
	public void createMultipleArticles(String title, String description, String body) {
		
		AngularUtils.waitForAngular(session.getDriver());
		
		newArticlePage.setTitle(title);
		newArticlePage.setDescription(description);
		newArticlePage.setBody(body);
		
		AngularUtils.waitForAngular(session.getDriver());
		
		newArticlePage.clickPublishButton();
		
		AngularUtils.waitForAngular(session.getDriver());
		
		headerBarComponent.clickNewArticle();
		
		PageSelection.goToPage(session, "home");
		
		AngularUtils.waitForAngular(session.getDriver());
		
		homePage.selectFeed("global");
		
		AngularUtils.waitForAngular(session.getDriver());
		
		assertTrue(homePage.containsArticle(title));
		
		headerBarComponent.clickNewArticle();
	}
}
