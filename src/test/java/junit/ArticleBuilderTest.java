package junit;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import datagenerators.ArticleBuidler;
import dataobjetcs.Article;

/**
 * 
 * @author Timothy Lks
 *
 *         Test to check the Article Builder
 */
public class ArticleBuilderTest {
	
	@Test
	public void successfulArticleBuild() {
		String expectedTitle = "test title";
		String expectedDescription = "test description";
		String expectedBody = "test body";
		String[] expectedTags = { "tag1", "tag2" };
		
		Article article = ArticleBuidler.getArticle().withTitle(expectedTitle).withDescription(expectedDescription)
				.withBody(expectedBody).withTags(expectedTags).build();
		
		assertThat(article.getTitle()).isEqualTo(expectedTitle);
		assertThat(article.getDescription()).isEqualTo(expectedDescription);
		assertThat(article.getBody()).isEqualTo(expectedBody);
		assertThat(article.getTags()).isEqualTo(expectedTags);
	}
}
