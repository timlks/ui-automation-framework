package runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

// @formatter:off
	@RunWith(Cucumber.class)
	@CucumberOptions(
			features = "src/test/resources/features", 
			glue = { "steps" }, 
			tags = { "@test", "~@exploratory","~@bug" },
			plugin = { "pretty", "html:target/test-reports", "json:target/cucumber-report/cucumber-google-test.json" })
// @formatter:on

/**
 * Test Runner for the CCS Ingestion Retry tests
 * 
 *
 */
public class Runner {
	
}
