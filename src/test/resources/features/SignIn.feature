@test
Feature: Signing in

  Background: 
    Given a user was successfully created

  Scenario: Successful sign in
    Given the user navigates to the "signIn" page
    When the user signs in with valid information
    Then the user is signed in
