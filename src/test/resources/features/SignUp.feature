@test
Feature: Signing up

  Scenario: Successful sign up
    Given the user navigates to the "signUp" page
    When the user adds valid information
    Then the user is signed in
