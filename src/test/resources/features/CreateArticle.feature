@test
Feature: Creating a new article

  Background: 
    Given a user was successfully created

  Scenario: Successful creation of an article
    Given the user signs in with valid information
    And the user navigates to the "newArticle" page
    When the user creates a new article with the following information
      | title       | test title       |
      | description | test description |
      | body        | test body        |
      | tags        | tag1,tag2        |
    And the user navigates to the "home" page
    And selects "global" feed
    Then the article is displayed
