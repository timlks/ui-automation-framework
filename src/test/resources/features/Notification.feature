@test
Feature: Creating a new article

  Background: 
    Given a user was successfully created

  Scenario: Error notification when trying to sign in
    Given a user was successfully created
    And the user navigates to the "signIn" page
    When the user enters wrong credentials
    Then a notification saying "email or password is invalid" is displayed
